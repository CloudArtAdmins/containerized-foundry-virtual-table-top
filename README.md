# Foundry Virtual Tabletop Container Image Builder
This repository is to provide the tools for setting up a container image for a Foundry Virtual Table Top image for personal use.

NOTE: Please make sure your download link is for the Linux version

# Usage
### Quick build: 
`build-with-buildah.sh "<foundry-website-download-url>"` \
OR \
`build-with-docker.sh "<foundry-website-download-url>"`

### Full usage explaination
Obtain the foundryvtt server's zip file. \
`wget -O foundryvtt.zip "<foundry-website-download-url>"`

Extract the zip file to this repository.  \
`unzip -d /path/to/this/repo/foundryvtt foundryvtt.zip`

Build the container image using the repo's Dockerfile \
Buildah: `buildah bud Dockerfile` \
Docker: `docker image build Dockerfile`

Foundry servers use port 30000. \
Example script, yaml, and docker-composes may be added to this repo at a later date.
