#!/bin/sh

# Get server files
mkdir foundryvtt
wget -O foundryvtt.zip $1 &&

# extract
unzip -d foundryvtt foundryvtt.zip &&

# build the image
docker image build Dockerfile
