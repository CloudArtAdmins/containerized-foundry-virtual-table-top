#!/bin/sh

#podman run -it --rm -p 30001:30000 
podman pod create \
	--network proxy-net \
	--name foundry-pod

podman run \
	--detach \
	--pod foundry-pod \
	--volume foundrydata:/home/node/foundrydata:z \
	--restart always \
	--name foundry \
	$1
